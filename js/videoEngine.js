/*jshint multistr: true */
var margin = 20;
var cols = 1;
var rows = 1;
anim_totalUsers = 0;
var supersize = undefined;
var supersizeCoeff = 1.6;

var animateEntry = function(userNumber, umbWidth, umbHeight, leftCenter, topCenter){
	$( '#umb'+userNumber ).animate({
		width: umbWidth,
		height: umbHeight,
		left: leftCenter,
		top: topCenter
	}, 300 );
};

var generateUMB = function(umbID, stream, userID, userName, vidOverlay, audOverlay){
		var umb = $("<div></div>");
		umb.addClass("umb");
		umb.attr("id", "umb"+umbID);
		umb.attr("data-userid", userID);

		if(userID === 'localVideo'){
			var haveSound;
			if(typeof easyrtc.localStream !== 'undefined' && easyrtc.localStream.getAudioTracks().length === 0){
				haveSound=false;
			} else {
				haveSound = true;
			}

			umb.addClass("localVideo");
			umb.addClass("localMedia");

			umb.html('\
				<div class="info">\
					<span class="userName">' + userName + '</span>\
					<button class="localMuteAudio"'+ (haveSound?'':' style="display: none"')+ '>Mute Audio</button>\
					<button class="localPauseVideo">Pause Video</button>\
					<button class="fullscreen">Full Screen</button>\
				</div>   \
				<div class="videoWrap">\
					<div class="audioOverlay"></div>\
					<div class="videoOverlay"><span></span></div>\
					<video muted autoplay src="' + stream + '" ></video>\
				</div>\
			');
			
			loadModal();

			umb.hide();
		} else {
			
			
			if(dartUtil.metadata.isVoiceCall(userID)){	
				//add an audio user
				umb.addClass("remoteAudio");
				umb.addClass("audioElementContainer");
				umb.html('\
					<div class="audioWrap' + (audOverlay?' audOverlay':'') +' ">\
						<div class="audioUserMuted"></div>\
						<div class="audioUser"></div>\
						<div class="userName">' + userName + '</div>\
						<video autoplay src="' + stream + '" style="display: none"></video>\
					</div>\
				');				
			} else {
				//add a normal video user
				umb.addClass("remoteVideo");
				umb.html('\
					<div class="info">\
						<span class="userName">' + userName + '</span>\
						<button class="userDisconnect"' + (dartUtil.alphausr?'':' disabled="true" style="display: none"') +'>Disconnect</button>\
						<button class="userMuteAudio"' +  (typeof easyrtc.peerConns[userID].stream.getAudioTracks()[0] === 'undefined' ? "style='display: none'" : "")  + '>Mute Audio</button>\
						<button class="userPauseVideo">Pause Video</button>\
						<button class="fullscreen">Full Screen</button>\
					</div>\
					<div class="videoWrap">\
						<div class="masterVideoOverlay"></div>\
						<div class="audioOverlay"></div>\
						<div class="videoOverlay"><span></span></div>\
						<video autoplay src="' + stream + '" ></video>\
					</div>\
				');
				if(audOverlay) $(umb).find('.audioOverlay').fadeIn();
				if(vidOverlay) $(umb).find('.videoOverlay').fadeIn();
			}
		}
		return umb;
};

var addMediaUser = function(stream, userID, name, vidOverlay, audOverlay){
	if (anim_totalUsers == 4){
		return;
	}
	anim_totalUsers++;
	
	umb = generateUMB(anim_totalUsers, stream, userID, name, vidOverlay, audOverlay);
	if(userID == "localVideo"){
		$("#mediaContainer").append(umb);	
	} else {
		$("#mediaContainer").append(umb);
	}
	fixAll();	
	fixUser(anim_totalUsers);
	
};

/*
	Behavior of fixAll [ excluding supersize functionality ]
	
	cols = 1 | when | anim_totalUsers 1, 
					anim_totalUsers2 && totalHeight>totalWidth
					anim_totalUsers3 && totalHeight>totalWidth
	cols = 2 | when | anim_totalUsers 2 && totalWidth>availableHeight, 
					anim_totalUsers 4 && umb1 - 4
	cols = 3 | when | anim_totalUsers 3 && totalWidth/totalHeight > 4/3

	rows = 2 | when | anim_totalUsers 3 && umb1 && umb2 && totalWidth/totalHeight == 4/3

	extraHeight | when | umb2 && anim_totalUsers 2 && totalHeight>totalWidth
						umb2 && anim_totalUsers3 && totalWidth/totalHeight == 4/3
						umb2 && anim_totalUsers3 && totalHeight>totalWidth + [2] umb3 && totalHeight>totalWidth
						umb3-4 && anim_totalUsers4

	extraWidth | when | umb2 && anim_totalUsers2 && totalWidth>totalHeight
						umb3 && anim_totalUsers3 && totalWidth/totalHeight == 4/3
						umb2 && anim_totalUsers3 && totalWidth/totalHeight > 4/3 + [2] umb3 && anim_totalUsers3 && totalWidth/totalHeight > 4/3
						umb2-4 && anim_totalUsers4 

	width	|	when	| anim_totalUsers1 
						anim_totalUsers2 && totalHeight>totalWidth

	width/2 |	when	| anim_totalUsers2 && totalWidth>totalHeight
						anim_totalUsers3 && totalWidth/totalHeight == 4/3
						anim_totalUsers 4 

	width/3 |	when	| anim_totalUsers 3 && totalWidth/totalHeight > 4/3
*/

var supersizeUMB = function(num) {
	if(anim_totalUsers == 1){
		return;
	}
	if(typeof supersize !== 'undefined'){
		unsupersizeUMB();
	}	
	supersize = {
		"umb": num		
	};
	fixAll();
};

var unsupersizeUMB = function(){
	if(typeof supersize === 'undefined'){
		return;
	}
	
	$('#umb'+supersize.umb).removeClass('supersized');
	supersize = undefined;
	fixAll();
};

var changeSupersize = {
	newValue: function(size){
		var newsize;
		if(typeof size === 'string'){
			newsize = parseFloat(size);
		} else {
			newsize = size;
		}
		if(newsize > 1 && newsize < 1.9){
			supersizeCoeff = newsize;
			fixAll();
		} else {
			dartUtil.log('Invalid supersize value');
		}		
	},
	defaultValue: function(){
		supersizeCoeff = 1.6;
		fixAll();
	}
};

var fixUser = function(num){

	var totalWidth = $("#mediaContainer").outerWidth();
	var totalHeight = $("#mediaContainer").outerHeight();
	var availableWidth = 0;
	var availableHeight = 0;
	var extraHeight = 0;
	var extraWidth = 0;
	
	if(typeof supersize !== 'undefined'){
		if(supersize['umb'] == num){
			availableWidth = totalWidth/2 * supersizeCoeff;
			availableHeight = totalHeight;
			margin = 50;						
		} else {
			margin = 10;
			availableWidth = totalWidth/2 * (2 - supersizeCoeff);
			availableHeight = totalHeight/(anim_totalUsers - 1);
			if(num >  supersize.umb){
				extraHeight = availableHeight * (num - 2);	
			} else {
				extraHeight = availableHeight * (num - 1);	
			}			
			extraWidth = (totalWidth/2 * supersizeCoeff)- (margin/2);
		}
	} else {
		margin = 20;
		if(totalWidth<totalHeight){
			cols = 1;
			rows = anim_totalUsers;
			availableHeight = totalHeight / rows;
			availableWidth = totalWidth / cols;
			extraHeight = availableHeight * (num - 1);
		} else if(anim_totalUsers == 4){
			rows = cols = 2;
			availableHeight = totalHeight / rows;
			availableWidth = totalWidth / cols;
			if(num > 2 ? extraHeight=availableHeight : extraHeight=0 ); // add height only if its umb 3 or 4
			extraWidth = availableWidth * (1-num%2); // add width only if its umb is 2 or 4 = even
		} else if(anim_totalUsers == 3){
			if(totalWidth/totalHeight > 1.4){
				cols = anim_totalUsers;
				rows = 1;
				availableHeight = totalHeight / rows;
				availableWidth = totalWidth / cols;
				extraWidth = availableWidth * (num - 1);
			} else {
				cols = 2;
				availableWidth = totalWidth / cols;
				if(num==3){
					rows=1;
					extraWidth = availableWidth;
					margin = 50;
				} else {
					rows=2;
					if(num == 2){
						extraHeight = totalHeight / rows;
					}
				}			
				availableHeight = totalHeight / rows;						
			}
		} else {
			rows = 1;
			cols = anim_totalUsers;
			availableHeight = totalHeight / rows ;
			availableWidth = totalWidth / cols;
			extraWidth = availableWidth * (num - 1);
		}	
	}
	var umbWidth = Math.min(availableWidth,availableHeight) - margin;
	var umbHeight = umbWidth * (3 / 4);
	
	if(availableHeight-margin-10 > umbHeight){
		if(availableWidth - 10*margin > umbWidth){
			umbWidth = umbWidth + availableHeight-margin-umbHeight; 
			umbHeight = umbWidth * (3 / 4);
		}
	}	


	var leftCenter = availableWidth/2 - umbWidth/2 + extraWidth;
	var topCenter = availableHeight/2 - umbHeight/2 + extraHeight;

	animateEntry(num, umbWidth, umbHeight, leftCenter, topCenter);
};


var fixAll = function(){
	if ($('#umb1').length > 0) fixUser(1);				
	if ($('#umb2').length > 0) fixUser(2);				
	if ($('#umb3').length > 0) fixUser(3);				
	if ($('#umb4').length > 0) fixUser(4);
	console.log("done resizing");	
};

var anim_entry = function(stream, userID, name, vidOverlay, audOverlay){
	addMediaUser(stream, userID, name, vidOverlay, audOverlay);
};

var anim_remove = function(userID){
	dartUtil.log("Video Engine | removing video of ID: " + userID);
	umbID = $('#mediaContainer').find('[data-userid="'+userID+'"]').attr('id');

	$('#'+umbID).remove();
	var umbIdNum = parseInt(umbID[3]);
	if(typeof supersize !== 'undefined'){
		if(supersize.umb === umbIdNum) unsupersizeUMB();
	}	
	if(anim_totalUsers > umbIdNum){
		if(typeof supersize !== 'undefined'){
			supersize.umb--;
		}
		for(var i=anim_totalUsers; i > umbIdNum; i--){
			console.log("decremeting umb: " + i);
			$('#umb'+i).attr('id', 'umb'+ (i - 1));
		}
	}	
	anim_totalUsers--;
	if(typeof supersize !== 'undefined'){
		if(anim_totalUsers == 1) unsupersizeUMB();
	}
	fixAll();
};