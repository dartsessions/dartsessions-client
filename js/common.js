var defaultTitle = document.title;

var titleBlinker = "";

var currentRandomUserName = "";

var nudgeToken = "[nudge]";

var started = false;

var roomName;
var isActive;
var checkpoint1 = false;

connectionTimeout = 6660000;

var messageTextBox = $('#messageTextBox');


// Start Custom jQuery Functions

jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
};

jQuery.fn.toggleSelector = function () {
	this.fadeToggle(200);
    return this;
};

jQuery.fn.pressToggler = function (targetSelector) {
	if ( this.hasClass("pressed") ) {
		this.removeClass("pressed");
	} else {
		this.addClass("pressed");
	}
	
	if (targetSelector) {
		$(targetSelector).toggleSelector();
	}
    return this;
};


jQuery.fn.removeDefaultTextOnClick = function () {
	this.on("focus", function(){
		if(this.value==this.defaultValue)this.value='';
	});
	
	this.on("blur", function(){
		if(this.value === '')this.value=this.defaultValue;
	});	
};

jQuery.fn.mediaContainerWidthToggler = function(mycallback) {
	var finalWidth;
	if ( this.hasClass("pressed") ) {
		finalWidth = $( "#center" ).outerWidth() - $("#messageLogBox").outerWidth() - 5;
		$( "#mediaContainer" ).animate({width: finalWidth + "px"},10, function(){
			if (mycallback){
					mycallback();
			}
		});
	} else {
		finalWidth = $( "#center" ).outerWidth();
		$( "#mediaContainer" ).animate({width: finalWidth + "px"},10, function(){
			if (mycallback){
					mycallback();
			}			
		});
	}	
	
    return this;
};

// End Custom jQuery Functions


var triggerRadio = function(target) {
	
	if ( $(target).is(":enabled") ){
		$(target).prop("checked", !$(target).prop("checked")); 
		$(target).trigger('click');	
	}
};

var playSound = function(type) {

	if (type == "joinedRoom"){
	document.getElementById("audiotag1").play();	
	}

};	

var showOptions = function(){
	
	if ( $('.modalWindow.share').is(":visible")) {
		$('.modalWindow.share').fadeOut(300);			
	}	
	
	if ( $('.modalWindow.options').length === 0 ) { // modalWindow was never loaded to the DOM. Load for the for the first time
		var modalContainer = $("<div></div>");
		modalContainer.addClass("modalWindow options");
		modalContainer.prependTo("body");
		
		$('#loader').fadeIn(300);
	
		$('.modalWindow.options').load( "partials/modalOptions.php", function() {
			
			$('#loader').hide();
			$('.modalWindow.options').center();
			$('.modalWindow.options').fadeIn();		
			$('.modalWindow.options').draggable({
				containment: $('#center')
			});				
			
	
		});

	} else { // modalWindow was already loaded to the DOM
	
		if ( $('.modalWindow.options').is(":visible")) {
			$('.modalWindow.options').fadeOut(300);			
		} else {
			$('.modalWindow.options').fadeIn(300);
		}
	}

};


var showShare = function(){
	
	if ( $('.modalWindow.options').is(":visible")) {
		$('.modalWindow.options').fadeOut(300);			
	}
	
	if ( $('.modalWindow.share').is(":visible")) {
		$('.modalWindow.share').fadeOut(300);			
	} else {
		$('.modalWindow.share').center();
		$(".modalWindow.share").draggable({
			containment: $('body')
		});		
		$('.modalWindow.share').fadeIn();
	}
	

};

var showAndHideToolTip = function(targetElement) {
	$( targetElement ).tooltip( 'open' );
	
	setTimeout(function(){ 
		$( targetElement ).tooltip( 'close' );
	},6000);
};


$(document).on("click", '#audioCheckbox', function() {
	console.log("audioCheckbox clicked");
	if ( this.checked ) {
		document.getElementById('audiotag1').muted = false;
		setCookie("soundStatus", "enabled", 365);
		console.log("resumed");			
	} else {
		document.getElementById('audiotag1').muted = true;
		setCookie("soundStatus", "disabled", 365);
		console.log("muted");	

	}
});
	

var clip = new ZeroClipboard( document.getElementById("copy-button"), {
  moviePath: "js/lib/ZeroClipboard/ZeroClipboard.swf"
} );

clip.on( "load", function(client) {

  client.on( "complete", function(client, args) {
    // `this` is the element that was clicked
	dartUtil.ui.showAlertBox("Link Copied! Share it with someone.");
  } );
} );


function addUser()
{
	var win = window.open( $("#sessionLinkTextBox").val(), '_blank');
	win.focus();
}

keyboardEvents = function(evt,triggerButton) {
	if (evt.keyCode == 13){
		$(triggerButton).trigger("click");
	}
};

var generateRandomUserName = function() {

	var genericNames = ["Smith", "Anderson", "Clark", "Wright", "Mitchell", "Johnson", "Thomas", "Rodriguez", "Lopez", "Perez", "Williams", "Jackson", "Lewis", "Hill", "Roberts", "Jones", "White", "Lee", "Scott", "Turner", "Brown", "Harris", "Walker", "Green", "Phillips", "Davis", "Martin", "Hall", "Adams", "Campbell", "Miller", "Thompson", "Allen", "Baker", "Parker", "Wilson", "Garcia", "Young", "Gonzalez", "Evans", "Moore", "Martinez", "Hernandez", "Nelson", "Edwards", "Taylor", "Robinson", "King", "Carter", "Collins"];

	var randomNumber = Math.floor((Math.random()*49)+1);

	var randomNumber2 = Math.floor((Math.random()*9999)+1);

	var userID = genericNames[randomNumber] + randomNumber2;
	return(userID);
};


var goToStartPage = function() {
	var redirectTo = location.href.match( /^(http.+\/)[^\/]+$/ )[1];
	$("#globalContainer, .modalWindow, .ui-tooltip").fadeOut();

	setTimeout(function(){
		window.location = redirectTo;
	},1000);			
	
};

function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
		}
	}
}
	
var getID = function(){
	roomName = GetURLParameter('id');
};

var bindEvents = function() {
	// UI Button Bindings
		
	$("#usersButton").click( function(event) {
		$(this).pressToggler("#usersListBox"); 
		event.preventDefault();
	});
	

	$("#publicButton").click( function(event) {
		
		if ( $(this).hasClass("pressed") ) {
			dartUtil.sendMessage.server.disablePublicListing();			

		} else {
			dartUtil.sendMessage.server.enablePublicListing();

		}
		
	});	
	
	
	$("#messageLogButton").click( function() { 

		if ( $(this).hasClass("pressed") ) {
			$(this).removeClass("pressed");
		} else {
			$(this).addClass("pressed");
			var popout = $("#messagebuttonContainer").find(".popout");
			popout.removeClass("visible");
			popout.html("0");
			
		}
		
		$("#messageLogBox").toggleSelector();
		$("#messageTextBox").focus();		
		
		$(this).mediaContainerWidthToggler(function(){
			fixAll();
		});

		//blink public message stop
		$("#messageLogTextArea")[0].scrollTop = $("#messageLogTextArea")[0].scrollHeight;
	});


	$("#lockRoomButton").click( function() { 

		$("#lockRoomButton").attr('disabled','disabled');

		if ( $(this).hasClass("pressed") ) {
			roomLocked = false;
			dartUtil.sendMessage.server.unlockRoom();

		} else {			
			roomLocked = true;
			dartUtil.sendMessage.server.lockRoom();			
		}
	});
	
		
	$('#fullScreenButton').click(function () {
		$(this).pressToggler();		
		if (screenfull.enabled) {
			screenfull.toggle();
		}
	});
	
	$("#optionsButton").click( function() {  showOptions();  } );		
		
	$("#powerButton").on('click', function(){ 
		easyrtc.disconnect(); 
		goToStartPage();
	});
	

	$("#startControlsMuteAudio").on("click", function() {
		me = $(this);
		audioOverlay = $('.enterName .modalBody .audioOverlay');
	
		if ( me.hasClass("pressed") ) {
			dartUtil.mediaControls.unmuteLocal();			
			audioOverlay.fadeOut();
			dartUtil.isMuted = false;						
		} else {
			dartUtil.mediaControls.muteLocal();
			audioOverlay.fadeIn();
			dartUtil.isMuted = true;			
		}		
		me.pressToggler();				
	});	
		
	$("#startControlsPauseVideo").on("click", function() {
		me = $(this);
		
		videoOverlay = $('.enterName .modalBody .videoOverlay');
	
		if ( me.hasClass("pressed") ) {
			dartUtil.mediaControls.unpauseLocal();			
			videoOverlay.fadeOut();			
			dartUtil.isVideoPaused = false;
			
		} else {
			dartUtil.mediaControls.pauseLocal();
			videoOverlay.fadeIn();
			dartUtil.isVideoPaused = true;			
		}

		me.pressToggler();	
	});


	$(document).on("click", '.localMuteAudio', function() {
		me = $(this);
		audioOverlay = $('.localVideo .videoWrap .audioOverlay');
	
		if ( me.hasClass("pressed") ) {
			dartUtil.mediaControls.unmuteLocal();			
			audioOverlay.fadeOut();
			dartUtil.sendMessage.broadcast("unmute", "mediaControls");
			dartUtil.isMuted = false;						
		} else {
			dartUtil.mediaControls.muteLocal();
			audioOverlay.fadeIn();
			dartUtil.sendMessage.broadcast("mute", "mediaControls");			
			dartUtil.isMuted = true;			
		}				
		me.pressToggler();
	});	
		
	$(document).on("click", ".localPauseVideo", function() {
		me = $(this);
		
		videoOverlay = $('.localVideo .videoWrap .videoOverlay');
	
		if ( me.hasClass("pressed") ) {
			dartUtil.mediaControls.unpauseLocal();			
			videoOverlay.fadeOut();			
			dartUtil.sendMessage.broadcast("unpause", "mediaControls");			
			dartUtil.isVideoPaused = false;
			
		} else {
			dartUtil.mediaControls.pauseLocal();
			videoOverlay.fadeIn();
			dartUtil.sendMessage.broadcast("pause", "mediaControls");
			dartUtil.isVideoPaused = true;			
		}
		me.pressToggler();
	});	
	
	
	$(document).on("click", ".userbox .username", function() {
		$('.userbox .username').not(this).each(function(){
			$(this).closest(".userbox").removeClass('pressed');
		});
		var target = $(this).closest(".userbox");
		
		if ( target.hasClass("pressed") ) {
			target.removeClass("pressed");
		} else {
			target.addClass("pressed");
			target.find(".messageTextBox").focus();
			
			var messageLogTextArea = target.find('.messages');
			messageLogTextArea[0].scrollTop = messageLogTextArea[0].scrollHeight;			
			
			var popout = target.find(".popout");
			popout.removeClass("visible");
			popout.html("0");
		}		
	});	
	
	$(document).on("click", ".userbox .close", function() { 
		$(this).closest(".userbox").pressToggler();
	});			
	
	$(document).on("click", ".userbox .disconnect", function() { 
		var userid = $(this).closest(".userbox").data("userid");
		console.log("Kicking:" + userid);
		// send kicking logic
		dartUtil.sendMessage.private('dc', userid, 'disconnectMessage');				
	});				
	
	$(document).on("click", ".userbox .sendMessageButton", function() {
		messageTextBox = $(this).closest(".userbox").find('.messageTextBox');
		message = messageTextBox.val();
		
		if (message === "") {
			return;
		}
		
		messageTextBox.val("");
		
		var userid = $(this).closest(".userbox").data("userid");
		console.log("Sending Private message:" + message + " to:" + userid);
		
		dartUtil.ui.outputMessage(message, 'myPrivate', userid);		
		dartUtil.sendMessage.private(message, userid);
	});	
	
	$(document).on("click", ".userbox .sendNudgeButton", function() {

		var message = nudgeToken;
		
		var userid = $(this).closest(".userbox").data("userid");
		console.log("Sending Private message:" + message + " to:" + userid);
		
		dartUtil.ui.outputMessage(message, 'myPrivate', userid);		
		dartUtil.sendMessage.private(message, userid);		
	});		
	
	$(document).on("keydown", ".userbox .messageTextBox", function(event) { 
		keyboardEvents(event, $(this).closest(".userbox").find('.sendMessageButton'));	
	});
		
	$("#startControlsUserName").on("keydown", function(event) {
		
		if (!started){
			keyboardEvents(event, "#startButton");
		}
	});				
	
	$("#messageTextBox").removeDefaultTextOnClick();	

	
	$("#addUserButton").click( function() {
		showShare();
	});	
	
	
	$("#sendMessageButton").on('click', function(){
			message = $('#messageTextBox').val();
			if (message === "") {
				return;
			}	
			dartUtil.sendMessage.broadcast(message, "chat");
			dartUtil.ui.outputMessage(message,'myPublic');
			$("#messageTextBox").val("");
			$("#messageTextBox").focus();
		}	
	);
	
	$("#messageTextBox").on("keydown", function(event) {
		keyboardEvents(event, "#sendMessageButton");
	});	

	$(document).on("click", '.modalClose', function() {
			var me = $(this);
			var currentModal = me.closest('.modalWindow'); 
			currentModal.fadeOut();
	});
	
	$(document).on("click", '.userMuteAudio', function() {
			var me = $(this);
			me.pressToggler();					
			var remoteID = me.closest('.umb').data('userid');
			dartUtil.log(remoteID);
			if(me.hasClass("pressed")){
				dartUtil.log("Muting: " + remoteID);
				dartUtil.mediaControls.muteRemote(remoteID);
			} else {
				dartUtil.log("Unmuting: " + remoteID);
				dartUtil.mediaControls.unmuteRemote(remoteID);
			}
	} );
	
	$(document).on("click", '.userPauseVideo', function() {
			var me = $(this);
			me.pressToggler();
			var remoteID = $(this).closest('.umb').data('userid');
			dartUtil.log(remoteID);
			if(me.hasClass("pressed")){
				dartUtil.log("Pausing: " + remoteID);
				dartUtil.mediaControls.pauseRemote(remoteID);
				$("#mediaContainer").find('[data-userid="'+remoteID+'"]').find(".masterVideoOverlay").fadeIn();
			} else {
				dartUtil.log("Unpausing: " + remoteID);
				dartUtil.mediaControls.unpauseRemote(remoteID);
				$("#mediaContainer").find('[data-userid="'+remoteID+'"]').find(".masterVideoOverlay").fadeOut();
			}
	} );
	
	$(document).on("click", '.userDisconnect', function() {
			var me = $(this); 
			var remoteID = me.closest('.umb').data('userid');
			dartUtil.sendMessage.private('dc', remoteID, 'disconnectMessage');
			dartUtil.log("Just sent disconnection message to user");			
			
	} );
	
	$(document).on('click', '.localMedia .audioUser', function(){
		var me=$(this);
		if(me.hasClass('pressed')){
			dartUtil.mediaControls.unmuteLocal();
			dartUtil.log('Unmuted Local Media');
			me.removeClass('pressed');
			dartUtil.sendMessage.broadcast("unmute", "mediaControls");
			dartUtil.isMuted = false;
		} else {
			dartUtil.mediaControls.muteLocal();
			dartUtil.log('Muted Local Media');
			me.addClass('pressed');
			dartUtil.sendMessage.broadcast("mute", "mediaControls");
			dartUtil.isMuted = true;						
		}
	});
	
	$(document).on('click', '.remoteAudio .audioUser', function(){
		var me = $(this);
			me.pressToggler();
			var remoteID = $(this).closest('.umb').data('userid');
			dartUtil.log(remoteID);
			if(me.hasClass("pressed")){
				dartUtil.log("Muting: " + remoteID);
				dartUtil.mediaControls.muteRemote(remoteID);				
			} else {
				dartUtil.log("Unmuting: " + remoteID);
				dartUtil.mediaControls.unmuteRemote(remoteID);
			}
	});
	
	
	$('#spectatorOptionBox').click(function () {
								if ( this.checked ) {
									dartUtil.isSpectator = true;
									$("#spectatorOverlay").show();		
									dartUtil.log('Spectator mode checked | no media will be shared');
									
									//undo voiceOnly
									if(dartUtil.isVoiceCall){
										dartUtil.isVoiceCall = false;		
										$("#voiceCallOverlay").hide();
										$("#startControlsPauseVideo").show();										
									}
								}
							}
	);
	
	$('#voiceOnlyOptionBox').click(function () {
								if ( this.checked ) {
									dartUtil.isVoiceCall = true;
									$("#voiceCallOverlay").show();				
									$("#startControlsPauseVideo").hide();
									dartUtil.log('VoiceCall mode checked | only voice will be shared');
									
									//undo spectator
									if(dartUtil.isSpectator){
										dartUtil.isSpectator = false;		
										$("#spectatorOverlay").hide();
										dartUtil.log('Spectator mode un-checked | normal video call will be initiated');
									}
								}	
							}
	);	
	$('#regularOptionBox').click(function () {
								if ( this.checked ) {
									//undo spectator
									if(dartUtil.isSpectator){
										dartUtil.isSpectator = false;		
										$("#spectatorOverlay").hide();
										dartUtil.log('Spectator mode un-checked | normal video call will be initiated');
									}
									
									//undo voice
									if(dartUtil.isVoiceCall){
										dartUtil.isVoiceCall = false;		
										$("#voiceCallOverlay").hide();
									}
									
									$("#startControlsPauseVideo").show();
								}
							}
	);
	

	$(document).on('click', '.umb video, .umb .videoOverlay', function(){
		
		var me = $(this).closest('.umb');		
		
					
		if(me.hasClass('supersized')){
			unsupersizeUMB();
			me.removeClass('supersized');			
		} else {
			umbnumber = me.attr('id');
			
			if(typeof supersize !== 'undefined'){
				$('#umb'+supersize.umb).removeClass('supersized');
			}
			switch(umbnumber){
				case 'umb1': 
					supersizeUMB(1);
					break;
				case 'umb2':
					supersizeUMB(2);
					break;
				case 'umb3':
					supersizeUMB(3);
					break;
				case 'umb4':
					supersizeUMB(4);				
			}
			me.addClass('supersized');
		}
		
		
	});
	
};	// End Bindings		

function doneResizing(){
	fixAll();	
}

var resizeId;		
$(window).resize(function(e) {
	$('#alertBox').center();	
	$('.modalWindow.enterName').center();	
	$('.modalWindow.options').center();	
	$('.modalWindow.share').center();	
	$("#messageLogButton").mediaContainerWidthToggler(function(){
		clearTimeout(resizeId);
		resizeId = setTimeout(doneResizing, 100);
	});	
});

$(window).on("focus", function() {
	isActive = true;
	stopTitleBlinking();
});

$(window).on("blur", function() {
  isActive = false; 
});

window.onbeforeunload = function(e){
	easyrtc.disconnect();
};


$(document).on("click", '.fullscreen', function() {
	var target = $(this).closest(".umb").find("video")[0];
	if (screenfull.enabled) {
		screenfull.toggle(target);
	}
});	

$(document).ready(function() {
	
	if (bowser.msie || bowser.safari) {
		dartUtil.ui.showAlertBox("Your browser isn't supported yet. We only support Chrome, Firefox and Opera at the moment. Sorry!", 999999, function(){
			}
		);
		return;
	}		
	
	$("#startButton").removeAttr("disabled");
	started = false;
	initializeMedia();
	console.log( "ready!" );
	
	$("#audiotag1")[0].volume = 0.2;

		
	$( "#spectatorTooltip" ).tooltip({
      position: {
        my: "bottom+80",
        at: "center",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
    },{ show: {delay: 10} });
	

	$( "#voiceTooltip" ).tooltip({
		position: {
			my: "bottom+80",
			at: "center",
			using: function( position, feedback ) {
				$( this ).css( position );
				$( "<div>" )
				.addClass( "arrow" )
				.addClass( feedback.vertical )
				.addClass( feedback.horizontal )
				.appendTo( this );
			}
		}
	},{ show: {delay: 10} });

	$( "#regularTooltip" ).tooltip({
      position: {
        my: "bottom+60",
        at: "center",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
    },{ show: {delay: 10} });


	
    $( "#sessionLink" ).tooltip({
      position: {
        my: "center bottom-20",
        at: "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
    },{ show: {delay: 500} });
});


var loadModal = function(){
		dartUtil.log("Trace: init");
		
		getID();
		dartUtil.ui.setClientURL();
			
		$('#loader').fadeOut();
			
		// Start Modal Window Functions
				
		$('.enterName').center();
		$(".enterName").draggable({
			containment: $('body')
		});		
		$('.enterName ').fadeIn(function(){
			$("#startControlsUserName").focus();
		});	
					
		
		$("#startControlsUserName").on("click", function(){
		
			event.stopPropagation();
	
			var me = $(this);		
			if (me.val() == currentRandomUserName){
				me.val("");
			}
						
		});
		
	
		$('html').click(function() {
			if ($("#startControlsUserName").val() === "") {
				$("#startControlsUserName").val(currentRandomUserName);
			}
		});


		
		$("#startButton").click( function() { 
			
			dartUtil.userName=$('#startControlsUserName').val();
			$("#startButton").attr("disabled", "disabled");
						
			$('.enterName ').fadeOut(); 
			$(".hideInitially").fadeIn();
			$('#globalContainer	').fadeIn();	
			
			setCookie("defaultUserName", dartUtil.userName, 365);			

			if ( $('#spectatorOptionBox').is(':checked') ) { 
			
				setCookie("defaultMode", "spectator", 365);
				
			} else if ( $('#voiceOnlyOptionBox').is(':checked') ) { 
			
				setCookie("defaultMode", "audio", 365);				
				
			} else if ( $('#regularOptionBox').is(':checked') ) { 
			
				setCookie("defaultMode", "regular", 365);			
				
			}

			started = true;			
			

			if ( checkCookie("soundStatus") ) { 
				if (getCookie("soundStatus") == "enabled"){
					// enable audio
					document.getElementById('audiotag1').muted = false;	
				} else {
					// disable audio
					document.getElementById('audiotag1').muted = true;						
				}
			} else {
				document.getElementById('audiotag1').muted = false;	// enable audio by default // user never changed the default or is first time user
			}
			init();
		}); // End Modal Window Functions
				
		bindEvents();

		/*
		* Primary Media Check Barrier - checking available media and forcing appropriate modes
		*/
		if(easyrtc.localStream){
			if(easyrtc.localStream.getVideoTracks().length === 0 && easyrtc.localStream.getAudioTracks().length === 0){
				//no webcam and no sound - force spectator
				$('#voiceOnlyOptionBox').attr('disabled',true);
				$('#regularOptionBox').attr('disabled',true);
				triggerRadio('#spectatorOptionBox');
				
			} else if(easyrtc.localStream.getVideoTracks().length === 0){
				//no webcam - force audio user
				$('#regularOptionBox').attr('disabled',true);
				$('#spectatorOptionBox').attr('disabled',true);
				triggerRadio('#voiceOnlyOptionBox');				
								
			} else if(easyrtc.localStream.getAudioTracks().length === 0){				
				//no sound - force normal mode without any mute abilities
				$('#voiceOnlyOptionBox').attr('disabled',true);

				dartUtil.ui.showAlertBox('No Audio Source Detected', 2000, function(){
					$('#startControlsMuteAudio').remove();
					$('#voiceOnlyOptionBox').attr('disabled',true);
				});
			}
		} else {
			// no stream provided at all - force spectator
			$('#voiceOnlyOptionBox').attr('disabled',true);
			$('#regularOptionBox').attr('disabled',true);
			triggerRadio('#spectatorOptionBox');
		}
		
		


		/*
		* Cookie Stuff
		*/		

		if ( checkCookie("defaultUserName") ) { 
			$("#startControlsUserName").val(getCookie("defaultUserName"));
		} else {
			$("#startControlsUserName").val(generateRandomUserName());
			currentRandomUserName = $("#startControlsUserName").val();
		}
		

		if ( checkCookie("defaultMode") ) { 
			if (getCookie("defaultMode") == "regular"){

				triggerRadio("#regularOptionBox");
				
			} else if (getCookie("defaultMode") == "audio") {
				
				triggerRadio("#voiceOnlyOptionBox");
				
			} else if (getCookie("defaultMode") == "spectator") {
				
				triggerRadio("#spectatorOptionBox");
								
			}
			
		} else {
			
			triggerRadio("#regularOptionBox");
			
		}
				
};


easyrtc.setDisconnectListener(function (){
   easyrtc.disconnect();
   dartUtil.ui.showAlertBox("Connection to the server has been lost", 5000, function(){
		goToStartPage();
   });

});




var startTitleBlinking = function(){
	clearInterval(window.titleBlinker);	
	document.title = "Message Received";	
	window.titleBlinker = setInterval(function(){
		if (document.title == defaultTitle) {
			document.title = "Message Received";
		} else if (document.title == "Message Received") {
			document.title = defaultTitle;
		}
		
	},1000);
};


var stopTitleBlinking = function(){
	document.title = defaultTitle;
	clearInterval(window.titleBlinker);
	setTimeout(function(){
		document.title = defaultTitle;
	},1100);
	
};