String.prototype.replaceAll = function(str1, str2, ignore) 
{
   return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
};

var urlify = function(text) {
    var urlRegex = /(https?:\/\/[^\s]+)/g;
    return text.replace(urlRegex, function(url) {
        return '<a target="_blank" href="' + url + '">' + url + '</a>';
    });
};

var isOdd = function (num) { return num % 2;};

var smilify = function(msg){
	
	var arr = [
				":@","angry",
				":S","confused",
				"8)","cool", 
				"(6)","evil",
				":|","neutral",
				":(","sad",
				":O","shocked",
				":)","smile",
				":D","happy",
				":P","tongue",
				";)","wink"
				];
	
	for (i=0; i < arr.length - 1; i++){
		if (isOdd(i)){
			continue;
		} else {
			msg = msg.replaceAll(arr[i],'<span class="smiley '+  arr[i+1] + '"/>');
		}
	}	
	return msg;
};

easyrtc.setSocketUrl(signallingServer);

var appName = 'dartSessionsV1';

var isNewcomer = true;

easyrtc.enableDebug(false);
var dataToShip = "";//easyrtc fix for debug

easyrtc.enableDataChannels(true);

//----------------------------------------------------------------------------------------------------------message listeners start

var serverMessageListener = function(msgType, msgData, targeting) {
	console.log("The Server sent message type: " + msgType + " with data: " + msgData);
	if(msgType === 'successMessage'){
		//unstable
	} else if(msgType==='alphaMessage'){
		if(msgData.state==='on'){
			dartUtil.ui.alphausrui.set();
			dartUtil.ui.showAlertBox('Making you the session administrator', 2000, function(){
				}
			);		
			if(msgData.isPublic){
				//togglePublicSwitch
				$("#publicButton").pressToggler();
			}
			if(msgData.isLocked){
				//toggleLockSwitch
				$("#lockRoomButton").pressToggler();
			}
		} else {
			dartUtil.ui.showAlertBox('Lifting your Admin powers', 2000, dartUtil.ui.alphausrui.lift());
		}			
	}
};

var serverACKListener = function(msgType, msgData){
	if (msgType==='joinRoomPermit'){
		if(msgData === 'normalUser'){
			// normal user joined			
		} else if (msgData === 'adminUser'){
			dartUtil.ui.alphausrui.set();			
			
			dartUtil.ui.showAlertBox('Making you the session administrator', 2000, function(){
					showAndHideToolTip("#sessionLink");
				}
			);
			
		}
		easyrtc.joinRoom(roomName, null,
			function(roomName) {
				console.log("I'm now in room " + roomName);
				dartUtil.metadata.set();						
			},
			function(errorCode, errorText, roomName) {
				console.log("had problems joining " + roomName);						
			}
		);	
	} else if(msgType==='roomLocked'){
		dartUtil.ui.showAlertBox('The room is locked - Redirecting to home page', 3000, function(){
				goToStartPage();
			}
		);
	} else if(msgType==='roomFull'){
		easyrtc.disconnect();
		dartUtil.ui.showAlertBox('Room is full - Redirecting to home page', 3000, function(){
				$('#bottom').fadeOut();
				$('#top').find('li').fadeOut();
				$('#sessionLink').fadeOut();
				goToStartPage();
			}
		);
	} else if(msgType==='lockRoomACK' || msgType==='unlockRoomACK' ){
			$("#lockRoomButton").pressToggler();					
			$("#lockRoomButton").removeAttr('disabled');						
	}  else if(msgType==='enablePublicListingACK' || msgType==='disablePublicListingACK' ){
			$("#publicButton").pressToggler();
			$("#publicButton").removeAttr('disabled');						
	} 
	else {
		dartUtil.log('Server ACK received: <type: ' + msgType + ' data: ' + msgData + ' > [unhandled]');
	}
};

easyrtc.setServerListener( serverMessageListener );


var disableNudge = false;

var handleNudge = function(msgData,sendersEasyrtcid){
	
	if (disableNudge){
		return;
	}
	
	if (msgData == nudgeToken){
		disableNudge = true;
		target = $("#mediaContainer").find('[data-userid="'+sendersEasyrtcid+'"]');			
		target2 = $("#usersContainer").find('[data-userid="'+sendersEasyrtcid+'"]');				
		
		target.addClass('animated shake');
		target2.addClass('animated shake');
				
		setTimeout(function(){
			target.removeClass('animated shake');					
			target2.removeClass('animated shake');		
			disableNudge = false;
		},3000);				

	}	
};

var peerMessageListener = function(sendersEasyrtcid, msgType, msgData, targeting) {
	console.log("peer listener triggered");
	if( msgType === 'chat'){
		dartUtil.log( 'CHAT Message :' + msgData + " received");
		
		handleNudge(msgData,sendersEasyrtcid);
		
		dartUtil.ui.outputMessage(msgData, msgType, sendersEasyrtcid);
	} else if(msgType === 'privateChat'){
		dartUtil.log( 'PrivateCHAT Message :' + msgData + " received");
		
		handleNudge(msgData,sendersEasyrtcid);

		var userBox = $('#usersContainer').find('[data-userid="'+sendersEasyrtcid+'"]');
		
		if (isActive) {
			// tab is selected so dont start blinking
		} else {
			startTitleBlinking();
		}		
		
		if (userBox.hasClass("pressed")){
			// dont show popout
		} else { // update and show popout
			var popout = userBox.find(".popout"); 
			var newNumber = parseInt(popout.html()) + 1;
			popout.html(newNumber);
			popout.addClass("visible");
		}
		
		dartUtil.ui.outputMessage(msgData, msgType, sendersEasyrtcid);
		
	} else if( msgType === 'initStateCaller'){
		dartUtil.activePeerIDs[sendersEasyrtcid].isMuted=msgData.isMuted;
		dartUtil.activePeerIDs[sendersEasyrtcid].isVideoPaused=msgData.isVideoPaused;
		if(dartUtil.isSpectator){
			dartUtil.sendMessage.private('callSpec',sendersEasyrtcid,'connectToSpectator');
		} else {
			dartUtil.sendMessage.private(
											{	isMuted: dartUtil.isMuted, 
												isVideoPaused: dartUtil.isVideoPaused 
											},sendersEasyrtcid,'initStateCallee'
			);
		}
	} else if(msgType ==='initStateCallee'){
		dartUtil.activePeerIDs[sendersEasyrtcid].isMuted=msgData.isMuted;
		dartUtil.activePeerIDs[sendersEasyrtcid].isVideoPaused=msgData.isVideoPaused;
		console.log('Calling user from initStateCallee');
		performCall(sendersEasyrtcid);			
	} else if( msgType === 'connectToSpectator'){
		if( msgData === 'sendInitInfo'){
				dartUtil.sendMessage.private(
					{	isMuted: dartUtil.isMuted, 
						isVideoPaused: dartUtil.isVideoPaused 
					},sendersEasyrtcid,'initStateCaller'
				);
		} else if( msgData === 'callSpec'){
			console.log('Calling user from callSpec');
			performCall(sendersEasyrtcid);
		}
		dartUtil.log('a spectator asked to be called');
	} else if (msgType === 'mediaControls'){
		dartUtil.log('Media Controls accessed |' + msgData);
		if( $("#mediaContainer").find('[data-userid="'+sendersEasyrtcid+'"]').length > 0 ){
			switch(msgData){
			case "mute":
				dartUtil.ui.mediaControls.muteRemote(sendersEasyrtcid);
				break;
			case "unmute":
				dartUtil.ui.mediaControls.unmuteRemote(sendersEasyrtcid);
				break;
			case "pause":
				dartUtil.ui.mediaControls.pauseRemote(sendersEasyrtcid);
				break;
			case "unpause":
				dartUtil.ui.mediaControls.unpauseRemote(sendersEasyrtcid);				
			}	
		} else {
			console.log("Media Controls accessed before initial media state handshaking");
			dartUtil.activePeerIDs[sendersEasyrtcid].pendingMediaActions.push(msgData);
		}
			
	} else if( msgType === 'disconnectMessage'){
		if(msgData === 'dc'){
			easyrtc.disconnect();
			dartUtil.ui.showAlertBox('You just got kicked out of the session.', 3000, function(){
					goToStartPage();
				}
			);
		}
	}
};

//----------------------------------------------------------------------------------------------------------message listeners end

// streamAcceptor , listener triggered whenever a remote stream arrives
easyrtc.setStreamAcceptor( 
	function(callerEasyrtcid, stream) {
		if(dartUtil.activeVideoPeerIDs.indexOf(callerEasyrtcid)==-1){
			dartUtil.activeVideoPeerIDs.push(callerEasyrtcid);
		}
		dartUtil.ui.setRemoteVideo(callerEasyrtcid, stream);
		dartUtil.log(callerEasyrtcid + " connected");
	}
);

easyrtc.setOnStreamClosed( function (callerEasyrtcid) {
	if(!dartUtil.metadata.isSpectator(callerEasyrtcid)){
		dartUtil.ui.removeRemoteVideo(callerEasyrtcid);	
	} else {
		dartUtil.log('stopped sending stream to spectator: ' + callerEasyrtcid);
	}		
});

var initializeMedia = function(){
	easyrtc.setVideoDims(640,480);
	easyrtc.initMediaSource(
		function(){			// success callback
			dartUtil.ui.setLocalVideoModal();
			dartUtil.ui.setLocalVideo();
			dartUtil.ui.setConnectButton();
								
		},
		function(errorCode, errText){
			//set spec mode
			dartUtil.isSpectator=true;
			dartUtil.ui.setConnectButton();
			console.log(errText);
			loadModal();
		}
	);
};

var init = function() {
	
	$("#usersSelect").append("<option data-peerid="+ easyrtc.myEasyrtcid+">"+ dartUtil.userName +" [Me]"+"</option>");
		
	if(dartUtil.isSpectator){
		
		if (easyrtc.localStream){
			easyrtc.localStream.stop();
		}

		dartUtil.ui.removeLocalVideo();
		easyrtc.enableAudio(false);
		easyrtc.enableVideo(false);
		
	} else if(dartUtil.isVoiceCall || !easyrtc.localStream.getVideoTracks()[0]){
		dartUtil.ui.switchLocalToAudioUser();
		easyrtc.enableVideo(false);
		dartUtil.ui.showLocalVideo();
		dartUtil.isVoiceCall=true;
		if(dartUtil.isMuted){
			$('.localMedia .audioUser').addClass('pressed');
		}
	} else {
		dartUtil.ui.showLocalVideo();	
	}
	//set message receive listener
	easyrtc.setPeerListener( peerMessageListener );
	
	var connectSuccess = function(myId) {
		console.log("My easyrtcid is " + myId);				
		dartUtil.sendMessage.server.requestToJoinRoom();		
	};
	
	var connectFailure = function(errorCode, errText) {
		console.log(errText);
	};

	easyrtc.setUsername($('#startControlsUserName').val());
	
	easyrtc.connect(appName, connectSuccess, connectFailure);	
};
	


var roomListener = function (roomName, otherPeers) {
	if(isNewcomer){
		dartUtil.activePeerManagement.set(otherPeers);		
	} else {
		dartUtil.activePeerManagement.update();
	}
	for(var i in otherPeers) {
		dartUtil.log("----User---- " + i + " --");
		if(!otherPeers[i].apiField){
			dartUtil.log("----NO API---- for: " + i);			
			return;
		}
		dartUtil.metadata.get(i, otherPeers[i].apiField);
		if(!isNewcomer){
			var specObj = $('#usersContainer ul').find('[data-userid="'+i+'"]').find('.typeIcon');
			if (dartUtil.metadata.isSpectator(i)){
				if(!specObj.hasClass('spectator')){
					 specObj.removeClass('regular');
					 specObj.addClass('spectator');					 
				}
			} else if(dartUtil.metadata.isVoiceCall(i)){
				if(!specObj.hasClass('audio')){
					specObj.removeClass('regular');
					specObj.addClass('audio');
				}
			} else {
				if(!specObj.hasClass('regular')) specObj.addClass('regular');
			}
		}		
		//this segment runs only the first time the client connects to the room
		if(isNewcomer){
			if(dartUtil.isSpectator){	//if user is spectator, ask to be called
				if(!otherPeers[i].apiField.isSpectator.fieldValue){
					dartUtil.log('Asking non spectator users to call me');
					dartUtil.sendMessage.private('sendInitInfo', i,'connectToSpectator');
				}							
			} else {	//else start initializing a call
				console.log("sending message of type initStateCaller");
				dartUtil.sendMessage.private(
											{	isMuted: dartUtil.isMuted, 
												isVideoPaused: dartUtil.isVideoPaused 
											},i,'initStateCaller'
				);
			}
		}
		dartUtil.log('Browser: ' + JSON.stringify(otherPeers[i], null, 4));
		dartUtil.log("----------------------------------");
	}
	isNewcomer=false;
};

//setting a listener to listen to changes in the room
easyrtc.setRoomOccupantListener(roomListener);

var performCall = function (easyrtcid) {
	easyrtc.call(
	easyrtcid,
		function(easyrtcid, mediaType) { 
			dartUtil.log("completed call to " + easyrtcid + " of type: " + mediaType);
		}, function(errorCode, errorText){ 
			dartUtil.log(errorText, "<-!-> callFail |");
		}, function(accepted, bywho){
			dartUtil.log(("call " + accepted?"accepted":"rejected")+ " by " + bywho);

		}
	);
};

var leaveThisRoom= function(){
	easyrtc.leaveRoom(roomName, 
		function(roomName) {
			console.log("No longer in room " + roomName);
		},
		function(errorCode, errorText, roomName) {
			console.log("had problems leaving " + roomName);
		}
	);
};

easyrtc.setOnError(	//listening for errors GLOBAL STUFF
	function(errEvent) { 
		console.log(errEvent.errorText);	
	}
);

easyrtc.setAcceptChecker( function(sendersEasyrtcid, reporterFunction) {
		reporterFunction(true);									
});


/*******************************************************************************************************************
************************************************** Dart UTIL *******************************************************
*******************************************************************************************************************/
var dartUtil = {};
/*
*Variables
*/
dartUtil.userName = "Me";
dartUtil.activePeerIDs = {};
dartUtil.activeVideoPeerIDs = [];
dartUtil.isMuted = false;
dartUtil.isVideoPaused = false;
dartUtil.isVoiceCall = false;
dartUtil.isSpectator = false;
dartUtil.roomLocked = false;
dartUtil.alphausr = false;
dartUtil.enableLogs = true;
/*
*Functions
*/
//----------------------------------------------ActivePeerID Functions----------------------------------------------

dartUtil.activePeerManagement = {
		set: function(otherPeers){
			playSound('joinedRoom');
			dartUtil.log('setting activePeerIDs');
			dartUtil.activePeerIDs = {};
			for (var i in otherPeers){
				dartUtil.activePeerIDs[i] = { isMuted: false, isVideoPaused: false, pendingMediaActions: [] };
				dartUtil.activePeerManagement.makeUserBox(i);			
			}
		},
		update: function(){
			if(easyrtc.roomData[roomName].roomStatus=='update'){
				dartUtil.log('updating activePeerIDs');
				if(easyrtc.roomData[roomName].clientListDelta.removeClient){
					for(var i in easyrtc.roomData[roomName].clientListDelta.removeClient){
						if(dartUtil.activePeerIDs[i]){
							dartUtil.log('removing id: ' + i, '->');
							$('#usersContainer').find('[data-userid="'+i+'"]').remove();							
							delete dartUtil.activePeerIDs[i];							
						}
					}
				} else if(easyrtc.roomData[roomName].clientListDelta.updateClient){
					playSound('joinedRoom');
					for(var j in easyrtc.roomData[roomName].clientListDelta.updateClient){
						if(dartUtil.activePeerIDs[j]){
						} else {
							dartUtil.activePeerIDs[j] = { isMuted: false, isVideoPaused: false, pendingMediaActions: [] };
							dartUtil.log('adding id: ' + j, '->');
							dartUtil.activePeerManagement.makeUserBox(j);
						}
					}
				}
			}

		},
		refreshUsersList: function() {
			//refresh user number list
		},
		kickPeer: function(){
			
		},
		makeUserBox: function(id){
			var userType = "";
			if(dartUtil.metadata.isSpectator(id)){
				userType = "spectator";
			} else if(dartUtil.metadata.isVoiceCall(id)){
				userType = "audio";
			} else {
				userType = "regular";
			}
			
			$('#usersContainer ul').append('\
					<li class="userbox" data-userid="'+ id +'">\
						<div class="popout">0</div>\
						<div class="username">\
							<span>'+ easyrtc.idToName(id) +'</span>\
							<div class="typeIcon '+ userType +'"></div>\
						</div>\
						<div class="expandedForm">\
							<div class="options">\
								<button class="close" title="Close Chat Box"></button>\
								<button class="disconnect" title="Kick User" '+ (dartUtil.alphausr?'':' style="display: none"') +'></button>\
							</div>\
							<div class="messages ScrollStyle1">\
							</div>\
							<div class="formGroup">\
								<input name="messageTextBox" type="text" class="messageTextBox" value="">\
								<input type="button" value="Send Nudge" title="Send a Nudge" class="sendNudgeButton">\
								<input type="button" value="Send" "Send Message" class="sendMessageButton">\
							</div>\
						</div>\
                	</li>\
				');
		}
};

//---------------------------------------------User Interface Functions----------------------------------------------

dartUtil.ui = {
	removeRemoteVideo: function(id){
		if(dartUtil.activeVideoPeerIDs.indexOf(id)>-1){
			dartUtil.activeVideoPeerIDs.splice(dartUtil.activeVideoPeerIDs.indexOf(id),1);
		}
		//$("#mediaContainer").find('[data-userid="'+id+'"]').css( "opacity", "0");			
		dartUtil.log('remote video removed for: ' + id);
		anim_remove(id);
	},
	setLocalVideo: function(){
		anim_entry(URL.createObjectURL(easyrtc.localStream), "localVideo", dartUtil.userName, dartUtil.isVideoPaused, dartUtil.isMuted);
	},
	setLocalVideoModal: function(){
		var selfVideo = $(".enterName video")[0];
		easyrtc.setVideoObjectSrc(selfVideo, easyrtc.getLocalStream());
		selfVideo.muted = true;
		dartUtil.ui.setLocalVideoControls();			
	},
	showLocalVideo: function(){
		$(".localMedia").fadeIn();
		if (dartUtil.isVideoPaused){
			videoOverlay = $('.localVideo .videoWrap .videoOverlay');
			videoOverlay.fadeIn();
			$(".localPauseVideo").pressToggler();
		}
		
		if (dartUtil.isMuted){
			if(!dartUtil.isVoiceCall){
				audioOverlay = $('.localVideo .videoWrap .audioOverlay');
				audioOverlay.fadeIn();
				$(".localMuteAudio").pressToggler();
			} else {
				$('.localVideo').find('audioUser').addClass('pressed');
			}
		}		
	
		$(".localVideo .userName").html(dartUtil.userName + " [Me]");
		$(".localVideo").fadeIn();
		if(!dartUtil.isVoiceCall && $(".localVideo").find('video').get(0)) $(".localVideo").find('video').get(0).play();			
	},		
	removeLocalVideo: function(){
		anim_remove('localVideo');			
	},
	switchLocalToAudioUser: function(){
		dartUtil.log('Switching to audio user ui mode');
		var localAudioElement = $('.localVideo');
		localAudioElement.children().remove;
		localAudioElement.removeClass('localVideo');
		localAudioElement.addClass('localAudio');
		localAudioElement.addClass('audioElementContainer');
		localAudioElement.html('\
			<div class="audioWrap">\
				<div class="audioUser">\
				</div>\
				<div class="userName">'+ dartUtil.userName +'</div>\
			</div>\
		');		
	},
	setRemoteVideo: function(callerid, stream){
		anim_entry(URL.createObjectURL(stream), callerid, easyrtc.idToName(callerid), dartUtil.activePeerIDs[callerid].isVideoPaused, dartUtil.activePeerIDs[callerid].isMuted);			
		dartUtil.log('remote video successfully set for ' + callerid);
		
	},
	setLocalVideoControls: function(){
		var localMuteButton = $('#muteLocal');
		localMuteButton.on('click', function(){
									if(localMuteButton.hasClass('pressed')){
										dartUtil.mediaControls.unmuteLocal();
										localMuteButton.removeClass('pressed');
									} else {
										dartUtil.mediaControls.muteLocal();
										localMuteButton.addClass('pressed');
									}
								}
		);
		
		var localPauseButton = $('#pauseLocal');
		localPauseButton.on('click', function(){
									if(localPauseButton.hasClass('pressed')){
										dartUtil.mediaControls.unpauseLocal();
										localPauseButton.removeClass('pressed');
									} else {
										dartUtil.mediaControls.pauseLocal();
										localPauseButton.addClass('pressed');
									}
								}
		);
	},
	setConnectButton: function(){
		var connectButton = $('#connectButton');
		connectButton.removeAttr('disabled');
		connectButton.on('click', function(){
										$('#modalBody').hide();
										$('#appBody').removeAttr('style');
										init();											
									});
	},
	mediaControls:{
			muteRemote: function(id){
				if(!dartUtil.metadata.isVoiceCall(id)){
					$("#mediaContainer").find('[data-userid="'+id+'"]').find(".audioOverlay").fadeIn();
				} else {
					$("#mediaContainer").find('[data-userid="'+id+'"]').find(".audioWrap").addClass('audOverlay');
				}
				
			},
			unmuteRemote: function(id){
				if(!dartUtil.metadata.isVoiceCall(id)){
					$("#mediaContainer").find('[data-userid="'+id+'"]').find(".audioOverlay").fadeOut();
				} else {
					$("#mediaContainer").find('[data-userid="'+id+'"]').find(".audioWrap").removeClass('audOverlay');
				}
			},
			pauseRemote: function(id){					
				$("#mediaContainer").find('[data-userid="'+id+'"]').find(".videoOverlay").fadeIn();
			},
			unpauseRemote: function(id){
				$("#mediaContainer").find('[data-userid="'+id+'"]').find(".videoOverlay").fadeOut();
			},
			muteLocal: function(){
				$('.localVideo .videoWrap .audioOverlay').fadeIn();
			},
			unmuteLocal: function(){
				$('.localVideo .videoWrap .audioOverlay').fadeOut();
			},
			pauseLocal: function(){
				$('.localVideo .videoWrap .videoOverlay').fadeIn();
			},
			unpauseLocal: function(){
				$('.localVideo .videoWrap .videoOverlay').fadeOut();
			},			
	},
	outputMessage: function(message, type, id) {
		if	(!type){
			dartUtil.ui.displayOutputMessage(message, 'public');
			return;
		} else if (type == 'myPublic'){
			message = "<strong>You: </strong>"+message; 
			dartUtil.ui.displayOutputMessage(message, 'public', id);
			return;		
		} else if (type == 'myPrivate'){
			message = "<strong>You: </strong>"+message;
			dartUtil.ui.displayOutputMessage(message, 'private', id);
			return;
		} else if(type == "chat"){			
			var finalMessage = "<strong>" + easyrtc.idToName(id) +  "</strong>"  + ": " + message;
			
			dartUtil.ui.displayOutputMessage(finalMessage, 'public');		
			if ( $("#messageLogBox").is(":hidden") ){
				
				//blink public message
				var popout = $("#messagebuttonContainer").find(".popout");
				var newNumber = parseInt(popout.html()) + 1;
				popout.html(newNumber);
				popout.addClass("visible");				
			}	
			if (isActive) {
				// tab is selected so dont start blinking
			} else {
				startTitleBlinking();
			}
		} else if(type === 'privateChat'){
			var finalMessage = "<strong>" + easyrtc.idToName(id) +  "</strong>"  + ": " + message;
						
			dartUtil.ui.displayOutputMessage(finalMessage, 'private', id);		
		}			
	},		
	displayOutputMessage: function(msg, type, userid){
		messageLog = $("#messageLogTextArea");

		msg = smilify(msg);

		msg = urlify(msg);		
		
		var finalFormattedMessage = '<div class="singleMessage">' + msg +'</div>';
				
		if(type === 'public'){
			$("#messageLogTextArea").append(finalFormattedMessage);
			$("#messageLogTextArea")[0].scrollTop = $("#messageLogTextArea")[0].scrollHeight;
		} else if(type === 'private'){
			var messageLogTextArea = $('#usersContainer').find('[data-userid="'+userid+'"]').find('.messages');
			messageLogTextArea.append(finalFormattedMessage);
			messageLogTextArea[0].scrollTop = messageLogTextArea[0].scrollHeight;
		}
	},
	alphausrui: {
		set: function(){
			dartUtil.alphausr = true;
			$('.userDisconnect').removeAttr('disabled').fadeIn();
			$('.userBox .disconnect').fadeIn();
			$('#lockRoomButton').removeAttr('disabled').fadeIn();
			$('#publicButton').fadeIn();			
			
			$(document).on("dblclick", "option", function(){
				if($('body').attr('id')=='host'){
					var id=$(this).attr('data-peerid');
					if(id!=dartObject.peerID){
						dartUtil.sendMessage.private('dc', remoteID, 'disconnectMessage');
					}
				}
			});
		},
		lift: function(){
			dartUtil.ui.showAlertBox('Admin UI Turned OFF', 2000);
			dartUtil.alphausr = false;
			$('.userDisconnect').attr('disabled', 'true').fadeOut();
			$('#lockRoomButton').attr('disabled', 'true').fadeOut();
			$(document).on("dblclick", "option", function(){
					dartUtil.log('No permission to disconnect a user');					
				});
		}			
	},
	setUserVoiceCall: function(id){
		dartUtil.ui.mediaControls.pauseRemote(id);
		$("#mediaContainer").find('[data-userid="'+id+'"]').find(".userPauseVideo").attr('disabled', 'true').fadeOut();
	},
	setClientURL: function(){
		$("#sessionLinkTextBox").val(window.location.href);
	},
	showAlertBox: function(text,timeout,mycallback){
		var finalTimeout = 1000;
		if (timeout){
			finalTimeout = timeout;
		}
		if ($('#alertBox').length === 0){
			
			var alertBox = $("<div></div>");
			alertBox.attr("id", "alertBox");		
			alertBox.html(text);		
			alertBox.appendTo("#globalContainer").center().fadeIn(300);
			
			setTimeout(function(){ 
				$('#alertBox').fadeOut(300, function(){
					$('#alertBox').remove();
				});
			},finalTimeout);			
	}		
		if (mycallback){
			setTimeout(function(){ 
				mycallback();
			},finalTimeout);		
		}
	}
};
//-------------------------------------------------Custom Dart Log---------------------------------------------------
dartUtil.log = function(message, prefix){
	if(dartUtil.enableLogs){
		console.log((prefix?prefix+" ":"[dartSessions] : ") + message ); 
	}
};
//--------------------------------------------Message Sending Functions----------------------------------------------
dartUtil.sendMessage = {
	broadcast: function(message, typeOf){
			dartUtil.log("Broadcasting Message");
			
			var ackResponse = function(ackMesg) {
							console.log("saw the following acknowledgment " + JSON.stringify(ackMesg));
							if( ackMesg.msgType === 'error' ) {
								dartUtil.log(ackMesg.msgData.errorText, "<-!-> sendError |");
							}								
			};
			var type='chat';
			if(typeOf) type = typeOf;
			for(var i in easyrtc.lastLoggedInList[roomName]){
				if(i == easyrtc.myEasyrtcid) continue;
				easyrtc.sendData( i, type, message, ackResponse);
				dartUtil.log("sent to" + i, "  ->");					
			}			
	},
	private: function(message, destId, typeOf){
				var type='privateChat';
				if(typeOf) type = typeOf;
				
				if(typeOf === 'disconnectMessage'){
					if(!dartUtil.alphausr) return;
				}				
				easyrtc.sendData( destId, type, message, 
						function(ackMesg) {
						if( ackMesg.msgType === 'error' ) {
							console.log(ackMesg.msgData.errorText);
						}
					}
			);
	},
	server: {
		lockRoom: function(){
			if(!dartUtil.alphausr) return;
			easyrtc.sendServerMessage("lockRoom", { roomName: roomName }, 
				function(msgType, msgData){
					serverACKListener(msgType, msgData);
				}, function(errorCode, errorText){
					console.log("Error was " + errorText);
				});
		},
		unlockRoom: function(){
			if(!dartUtil.alphausr) return;
			easyrtc.sendServerMessage("unlockRoom", { roomName: roomName }, 
				function(msgType, msgData){
					serverACKListener(msgType, msgData);
			}, function(errorCode, errorText){
					console.log("Error was " + errorText);				
			});
		},
		requestToJoinRoom: function(){
			easyrtc.sendServerMessage("roomJoinRequest", { roomName: roomName }, function(msgType, msgData){
				serverACKListener(msgType, msgData);
			}, function(errorCode, errorText){});				
		},
		enablePublicListing: function(){
			if(!dartUtil.alphausr) return;
			easyrtc.sendServerMessage("enablePublicListing", { roomName: roomName }, function(msgType, msgData){
				serverACKListener(msgType, msgData);
			}, function(errorCode, errorText){});				
		},
		disablePublicListing: function(){
			if(!dartUtil.alphausr) return;
			easyrtc.sendServerMessage("disablePublicListing", { roomName: roomName }, function(msgType, msgData){
				serverACKListener(msgType, msgData);
			}, function(errorCode, errorText){});				
		},		
		
	}
};
//--------------------------------------------Metadata Handling Functions--------------------------------------------
dartUtil.metadata = {
	set: function(){
		easyrtc.setRoomApiField(roomName, "isSpectator", dartUtil.isSpectator);
		easyrtc.setRoomApiField(roomName, "isVoiceCall", dartUtil.isVoiceCall);
	},
	get: function(id, apiField){
		console.log(JSON.stringify(apiField.isSpectator, null, 4));
		console.log(JSON.stringify(apiField.isVoiceCall, null, 4));	
	},
	isMuted: function(id){
		if (easyrtc.lastLoggedInList[roomName][id]) {
			if (easyrtc.lastLoggedInList[roomName][id].apiField) {
				return easyrtc.lastLoggedInList[roomName][id].apiField.initiallyMuted.fieldValue;
			}
		}
		return false;			
	},
	isVideoPaused: function(id){
		if (easyrtc.lastLoggedInList[roomName][id]) {
			if (easyrtc.lastLoggedInList[roomName][id].apiField) {
				return easyrtc.lastLoggedInList[roomName][id].apiField.initiallyVideoPaused.fieldValue;
			}
		}			
		return false;			
	},
	isVoiceCall: function(id){
		if (easyrtc.lastLoggedInList[roomName][id]) {
			if (easyrtc.lastLoggedInList[roomName][id].apiField) {
				return easyrtc.lastLoggedInList[roomName][id].apiField.isVoiceCall.fieldValue;
			}
		}			
		return false;			
	},
	isSpectator: function(id){
		console.log("@inside isSpectator");
		if (easyrtc.lastLoggedInList[roomName][id]) {
			console.log("@isSpectator found roomName + ID");
			console.log(easyrtc.lastLoggedInList[roomName][id]);
			if (easyrtc.lastLoggedInList[roomName][id].apiField) {
				console.log("@isSpectator found roomName + ID + apiField");
				console.log("@isSpectator value: ",easyrtc.lastLoggedInList[roomName][id].apiField.isSpectator.fieldValue);
				return easyrtc.lastLoggedInList[roomName][id].apiField.isSpectator.fieldValue;
			}
		}			
		return false;			
	},
	update: function(){
		// update metadata
	},
};
//----------------------------------------------Video Control Functions----------------------------------------------
dartUtil.mediaControls = {
	muteLocal: function(){
		easyrtc.localStream.getAudioTracks()[0].enabled=false;
	},
	unmuteLocal: function(){
		easyrtc.localStream.getAudioTracks()[0].enabled=true;
	},
	pauseLocal: function(){
		easyrtc.localStream.getVideoTracks()[0].enabled=false;
	},
	unpauseLocal: function(){
		easyrtc.localStream.getVideoTracks()[0].enabled=true;
	},
	muteRemote: function(id){
		easyrtc.peerConns[id].stream.getAudioTracks()[0].enabled=false;
	},
	unmuteRemote: function(id){
		easyrtc.peerConns[id].stream.getAudioTracks()[0].enabled=true;
	},
	pauseRemote: function(id){
		easyrtc.peerConns[id].stream.getVideoTracks()[0].enabled=false;
	},
	unpauseRemote: function(id){
		easyrtc.peerConns[id].stream.getVideoTracks()[0].enabled=true;
	}
};