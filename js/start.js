var currentScrollTop = "";

var replaceAll = function(str, str1, str2, ignore) 
{
   return str.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
};



var url = protocol + signallingServer;

var currentRandomName = "";

(function startLoader() {

	$('#loader img').toggleClass("transparent");
	
	setInterval(function(){
		$('#loader img').toggleClass("transparent");	
	}, 500);

})();


$(window).load(function() {
	$('body').removeClass("preventScroll");
	$('html').removeClass("preventScroll");	
	$('#loader').fadeOut(500, function(){
		$("#section1 #logo").addClass("animated fadeInDown");			
		$("#section1 #navContainer").addClass("animated fadeInDown");
		$("#section1 .first").addClass("animated fadeInDown");
		$("#section1 #formContainer").addClass("animated fadeInDown");
		$("#section1 #shortInfo").addClass("animated fadeInUp");
		$("#section1 #footer").addClass("animated fadeInUp");
	});	
});


		
$(document).ready(function() {
	
	if (bowser.msie || bowser.safari) {
		alert("Your browser isn't supported yet. We only support Chrome, Firefox and Opera at the moment. Sorry!");
	}
	
	if (document.URL.indexOf("#feedback") > 1){
		$('#feedback').modal('show');
	} else if (document.URL.indexOf("#faq") > 1) {
		$('#faq').modal('show');
	}
	
	
	$("#feedbackForm").submit(function(event){
		event.preventDefault();			
	  console.log("Submitted");
	  var data = $('#feedbackForm').serialize();
	  console.log(data);
		$.ajax({
			url: 'http://27.147.180.252:8080/postFeedback',
			//url: 'http://27.147.203.130:8080/postFeedback',
			type: 'post',
			dataType: 'text',
			data: data,
			success: function(data) {
					alert(data);
					$('#feedback').modal('hide');
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("Error contacting signalling server.");
				console.log(thrownError);								
			}
		});
	});	
	
	
	$("#roomNameTextBox").on("click", function(event){
		event.stopPropagation();
		var me = $(this);		
		if (me.val() == currentRandomName) {
			me.val("");
		};
	});
	
	$('html').click(function() {
		if ($("#roomNameTextBox").val() == "") {
			$("#roomNameTextBox").val(currentRandomName);
		};
	});
		

	
	// Animate Section 1 Background 
	
	(function animateBG(){
		
		increment = 1;
		speed = 90;		
		
		(function startAnimation(){
			$('#animatedBG').animate({
				'background-position': '-=' +increment
			}, speed, 'linear',function(){
				startAnimation();
			});	
		})();	
				
	})();		
		
	var showRandomRoomName = function(){
		target = $("#roomNameTextBox");
		
		var finalURL = url + "/getRandomRoom"; 
	
		$.ajax({
			
			url:finalURL,
			success:function(result){
				var roomName = result.roomName;
				target.val(roomName);
				currentRandomName = roomName;
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("Could not reach server");
				showError("Server Down");
			}
			
		});			
	};
	
	if ( checkCookie("defaultRoomName") ) {
		$("#roomNameTextBox").val(getCookie("defaultRoomName"));
		currentRandomName = getCookie("defaultRoomName");
	} else {
		showRandomRoomName();				
	}
	
	if ( checkCookie("lastJoinedRoom") ) {
		$("#joinSessionRoomName").val(getCookie("lastJoinedRoom"));
	} else {
	}	
	
	
	var getPublicRooms = function(mycallback){
		target = $("#roomNameTextBox");
		
		var rows = '';
		
		var finalURL = url + "/getRoomList"; 

		$.ajax({
			
			url:finalURL,
			success:function(result){
				var c = 0;
				for (var i in result){ 
					c++;
					if (result[i] != 4){ // room found that has space
						rows = rows + '<tr><td>'+ c +'</td><td><a href="' + startPageURL + 'session.php?id=' + i + '">'+ i +'</a></td><td>' + result[i] + '/4</td></tr>';
					} else { // room is full
						rows = rows + '<tr><td>'+ c +'</td><td>'+ i +'</td><td>' + result[i] + '/4</td></tr>';							
					}

				}
				
				if (c == 0){ // no rooms
						rows = '<tr><td></td><td>No Public Sessions</td><td></td></tr>';
						$("#tableContainer tbody").html(rows);
						$("#section3 #empty").show();
				} else { // one or more rooms found
					$("#tableContainer tbody").html(rows);					
						$("#section3 #empty").hide();					
				}
				
				if(mycallback){
					mycallback();
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				rows = '<tr><td></td><td>No Public Sessions</td><td></td></tr>';
				$("#tableContainer tbody").html(rows);
				$("#section3 #empty").show();				
				console.log("Could not reach server", true);
			}
			
		});		
	};
	
	getPublicRooms();		
	
		
	
	$(".btn-refresh").on("click", function(){
		showRandomRoomName();
	});	
	
	$("#section3 .refreshIcon").on("click", function(){
		getPublicRooms(function(){
		});
	});	
	
	var specialChars = "<>@!#$%^&*()_+[]{}?:;|'\"\\,/~`="
	
	var check = function(string){
	 for(i = 0; i < specialChars.length;i++){
	   if(string.indexOf(specialChars[i]) > -1){
		   return true
		}
	 }
	 return false;
	}		
	
	var errorTimeout;
	
	var showError = function(msg){
		$("#loaderAnimation").hide();		
		$('.errorPopOver').attr('data-content', msg);
		$('.errorPopOver').popover('show');		
		
		clearTimeout(errorTimeout);
		errorTimeout= setTimeout(function(){
			$('.errorPopOver').popover('hide');	
		}, 2500);		
	};

	$(".btn-start").on("click", function(event){
		
		target = $("#roomNameTextBox");
							
		if (target.val() === "") {
			showError("Session Name Cannot be Empty");
			return;
		}		
		
		if ( check(target.val()) ){ // check for special characters
			showError("Session Name Contains Illegal Characters");
			return;			
		}
		
		
		var finalURL = url + "/checkRoom/" + target.val(); 
		
		$("#loaderAnimation").fadeIn();

		$.ajax({
			
			url:finalURL,
			success:function(result){
				var roomExists = result.roomExists;
				
				if (roomExists == "false") {
					event.preventDefault();			
					finalRoomName = $("#roomNameTextBox").val().replace(/\s+/g, '-').toLowerCase();						
					var redirectTo = startPageURL + "session.php?id=" + finalRoomName;
					setCookie("defaultRoomName", finalRoomName, 365);
					window.location = redirectTo;
				} else {
					showError("Sorry this room is already taken.");
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("Could not reach server");
				showError("Could Not Reach Server.");
			}
			
		});
		
	});	
	
	
	$("#btn-joinSession").on("click", function(event){
		event.preventDefault();
		
		target = $("#joinSessionRoomName");
		finalRoomName = target.val().replace(/\s+/g, '-').toLowerCase();						
		
		var finalURL = url + "/checkRoom/" + finalRoomName;
									
		if (target.val() === "") {
			alert("Session Name Cannot be Empty");
			return;
		}		
		
		if ( check(target.val()) ){ // check for special characters
			alert("Session Name Contains Illegal Characters");
			return;			
		}		
		
		$("#loaderAnimation").fadeIn();

		$.ajax({
			
			url:finalURL,
			success:function(result){
				var roomExists = result.roomExists;

				if (roomExists == "false") {
					event.preventDefault();
					alert("Sorry this session does not exist");
					$("#loaderAnimation").hide();
				} else {
					finalRoomName = $("#joinSessionRoomName").val().replace(/\s+/g, '-').toLowerCase();						
					var redirectTo = startPageURL + "session.php?id=" + finalRoomName;
					
					setCookie("lastJoinedRoom", finalRoomName, 365);
					
										
					window.location = redirectTo;					
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log("Could not reach server");
				alert("Could Not Reach Server");
				$("#loaderAnimation").hide();
			}
			
		});
		
				

	});		
	

	keyboardEvents = function(evt,triggerButton) {
		if (evt.keyCode == 13){
			$(triggerButton).trigger("click");
		}
	};
	
	$("#joinSessionRoomName").on("keydown", function(event) {
		keyboardEvents(event, "#btn-joinSession");
	});			
	
	$("#roomNameTextBox").on("keydown", function(event) {
		keyboardEvents(event, ".btn-start");
	});			
		
	// tooltips and popovers
	$('.hasToolTip').tooltip();
	
	$('#joinASession').on('shown.bs.modal', function (e) {
		$("#joinSessionRoomName").focus();			
	})
	
	// Smooth Scrolling
	$(function() {
	  $('a[href*=#]:not([href=#])').click(function(e) {
				  
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  if (target.length) {
			$('html,body').animate({
			  scrollTop: target.offset().top
			}, 500);
			return false;
		  }
		}
	  });
	});	
	
	// Scroll to Top
	$("#go-top, #symbol").on("click", function(){
				
		$('body,html').animate({
		  scrollTop: 0
		}, 500);	

	});

	$('#section1').waypoint(function(direction) {
		$('#nav').removeClass();
		$('#nav li').removeClass("selected");
		
		if (direction == "down"){
			$('#nav').addClass("section1");
			$('#homeLink').addClass("selected");			
		} else {
		
		}
						
	  
	},{ offset: 0 });		
	
	// Logo and animation scroll treatment
	$('#section2').waypoint(function(direction) {
		if (direction == "down"){
			$('#logo h1').fadeOut(300);
			$('#symbol').addClass("small");
			$('#section2 h2').addClass("animated fadeInDown");									
			$('#section2 .fourBlocks').addClass("animated fadeInDown");
			$('#section2 .illustration').addClass("animated bounceIn");			
			$('#section2 .features').addClass("animated fadeInDown");			

		} else {
			$('#logo h1').fadeIn(300);
			$('#symbol').removeClass("small");			
		}
		
		
	  
	},{ offset: 500 });	
		
	
	// Section 2 scroll treatment
	$('#section2').waypoint(function(direction) {
		$('#nav').removeClass();
		$('#nav li').removeClass("selected");

		if (direction == "down"){
			$('#nav').addClass("section2");			
			$('#howItWorksLink').addClass("selected");
			$('#go-top').fadeIn();
			
			
		} else {
			$('#nav').addClass("section1");
			$('#homeLink').addClass("selected");		
			$('#go-top').fadeOut();						
		}

	},{ offset: 55 });	
	
	// Section 3 scroll animations
	$('#section3').waypoint(function(direction) {
		if (direction == "down"){
			$('#section3 .container').addClass("animated bounceIn");			
		} else {

		}

	},{ offset: 500 });		
	
	// Section 3 scroll treatment:
	$('#section3').waypoint(function(direction) {
		$('#nav').removeClass();
		$('#nav li').removeClass("selected");

		if (direction == "down"){
			$('#nav').addClass("section3");
			$('#publicSessionsLink').addClass("selected");						
		} else {
			$('#nav').addClass("section2");			
			$('#howItWorksLink').addClass("selected");			
		}

	},{ offset: 0 });	
	
	
	// Section 4 scroll treatment
	$('#section4').waypoint(function(direction) {
		if (direction == "down"){
			$('#section4 .container').addClass("animated fadeInUp");			
		} else {

		}

	},{ offset: 500 });			
	
	// Section 4 scroll treatment:
	$('#section4').waypoint(function(direction) {
		$('#nav').removeClass();
		$('#nav li').removeClass("selected");

		if (direction == "down"){
			$('#nav').addClass("section4");
			$('#aboutLink').addClass("selected");						
		} else {
			$('#nav').addClass("section3");						
			$('#publicSessionsLink').addClass("selected");			
		}

	},{ offset: 0 });		


});	