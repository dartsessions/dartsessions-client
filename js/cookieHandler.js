function setCookie(cname,cvalue,exdays)
{
	var d = new Date();
	d.setTime(d.getTime()+(exdays*24*60*60*1000));
	var expires = "expires="+d.toGMTString();
	console.log("Cookie set:" + cname + "=" + cvalue + "; " + expires);
	document.cookie = cname + "=" + cvalue + "; " + expires;
}

function deleteCookie(cname)
{
	document.cookie = cname+"=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
}

function getCookie(cname)
{
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) 
	  {
	  var c = ca[i].trim();
	  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
	  }
	return "";
}

function checkCookie(cname)
{
	var cookieName=getCookie(cname);
	if (cookieName!="")
	{
		return true;
	}
	else 
	{
		return false;
	}
}