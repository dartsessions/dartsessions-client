<?php include 'config.php'; ?>

<!DOCTYPE html>
<html>
  <head>
    <title>DartSessions</title>
		
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
	<meta property="og:image" content="<?php echo $startPageURL ?>images/start/fbThumb.png" />
	<meta property="og:title" content="DartSessions"/>
    <meta property="og:description" content="I'm available for a DartSession!"/>
    
	<link href="css/global.css" rel="stylesheet" type="text/css" />
    <link href="css/animate.min.css" rel="stylesheet">    
    
    <script src="js/lib/jquery.min.js"></script>
    <script src="js/lib/jquery-ui.min.js"></script>
    <script src="js/lib/ZeroClipboard/ZeroClipboard.min.js"></script>  
    <script src="js/lib/bowser.min.js"></script>    
	<script src="js/lib/screenfull.js"></script>  
    
    <script>
		var signallingServer = "<?php Print($signallingServer); ?>";
		var protocol = "<?php Print($protocol); ?>";	
	</script>        

    <link rel="stylesheet" type="text/css" href="<?php Print($protocol); ?><?php Print($signallingServer); ?>/easyrtc/easyrtc.css" />
    <script src="<?php Print($protocol); ?><?php Print($signallingServer); ?>/socket.io/socket.io.js"></script>
    <script type="text/javascript" src="<?php Print($protocol); ?><?php Print($signallingServer); ?>/easyrtc/easyrtc.js"></script>    

    
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">    
    
	<?php include 'analytics.php'; ?>
    
  </head>

<body id="host">

<div id="loader"></div>

<div class="modalWindow enterName">
  <header><h2>Almost there</h2></header>
    <div class="modalBody">
        <div class="main respond">
            <h3>Enter any name for others to see</h3>
            <div class="formGroup">
              <input id="startControlsUserName" name="" value="Initiator" type="text" >
              <button id="startButton">Go!</button>
            </div>
            
            <label title="Regular Video Call">
            	<input id="regularOptionBox" name="UserType" type="radio" checked  /> 
                <span><strong>Video Call</strong></span>
            </label>

            <label title="No Video">
	            <input id="voiceOnlyOptionBox" name="UserType" type="radio"  />
                <span><strong>Voice Call</strong></span>
            </label>            
            
            <label title="No Audio or Video">
	            <input id="spectatorOptionBox" name="UserType" type="radio"  />
            	<span><strong>Text Chat</strong></span> | <span id="spectatorTooltip" title="You get to see others but they dont get to see you. However, you can still chat.">?</span>
            </label>            
        </div><!-- End #main -->
        <div class="side respond">
        	<div id="spectatorOverlay"></div>
        	<div id="voiceCallOverlay"></div>            
        	<div class="audioOverlay"></div>
        	<div class="videoOverlay"><span></span></div>            
            <video poster="images/noVideo.png" autoplay muted></video>
            <div id="startControls">
              <button id="startControlsMuteAudio">Mute Audio</button>
              <button id="startControlsPauseVideo">Pause Video</button>
            </div>
        </div><!-- End #side-->      
    </div><!-- End #modalBody -->
</div><!-- End #modalWindow -->


<div class="modalWindow share">
  <div id="wrapper">
      <header>
      <h2>Share</h2>
      <span class="modalClose">x</span>
      </header>
        <div class="modalBody">
            <div id="socialIcons">
            <?php include 'partials/addThis.php'; ?>
            </div>        
        </div><!-- End #modalBody -->
  </div><!-- End #wrapper -->

</div><!-- End #modalWindow -->


<div id="globalContainer">
    
    <div id="top" class="hideInitially">
        <img id="logo" src="images/logo.png" alt="DartSessions">
        <ul>
            <li><button title="Toggle Fullscreen" id="fullScreenButton">Fullscreen</button>
            <li><button title="Options" id="optionsButton">Options</button>
            <li><button title="Share and Invite" id="addUserButton">Add User</button>
            <li><button title="Disconnect" id="powerButton">Power</button>            
        </ul>
	    <button id="lockRoomButton" title="Lock Room">Lock Session</button>                
        <button id="publicButton" title="List your room in the Homepage" >Public Listing</button>                    
        <div title="Copy and share this link with others" id="sessionLink">
        	<input readonly type="text" id="sessionLinkTextBox" value="Getting Session Link..."/>
            <button id="copy-button" data-clipboard-target="sessionLinkTextBox" >Copy</button>
        </div>            
    </div><!-- End #top -->
    
    
    <div id="center">
        <div id="mediaContainer" class="scrollbar ScrollStyle1">
            <!-- contains all the videos -->
        </div><!-- End #mediaContainer -->
        
        <div id="usersListBox">
            <select name="usersSelect" multiple="multiple" id="usersSelect" class="ScrollStyle1">
            </select>    
        </div><!-- End #usersListBox -->
          
        <div id="messageLogBox">
            <div id="messageLogTextArea" class="ScrollStyle1"></div>
            <div class="formGroup">
                <input name="messageTextBox" type="text" id="messageTextBox" value=""/>    
		        <input type="button" value="Send" title="Send a message to everyone" id="sendMessageButton">
            </div>
        </div><!-- End #messageLogBox -->
    </div><!-- End #center -->
    
    <div id="bottom" class="hideInitially">
      <a id="usersButton" style="display: none" href="#">0</a>
	  <audio id="audiotag1" src="audio/notify.wav" preload="auto"></audio>      
        <div id="usersContainer">
            <ul>            
            </ul>
        </div>      
      
      <div id="messagebuttonContainer">
          <button id="messageLogButton">Session Messages</button>
          <div class="popout">0</div>
      </div>
      
    </div><!-- End #bottom -->
    
</div>

<script src="js/cookieHandler.js"></script>            

<script src="js/common.js"></script> 
<script src="js/videoEngine.js"></script>        
<script src="js/dartlogic.js"></script>  

</body>
</html>
