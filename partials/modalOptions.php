<script>
	if ( checkCookie("soundStatus") ) { 
		if (getCookie("soundStatus") == "enabled"){
			// enable audio
			$("#audioCheckbox").prop('checked', true);
		} else {
			// disable audio
			$("#audioCheckbox").prop('checked', false);
		}
	} else {
		$("#audioCheckbox").prop('checked', true);
	}
</script>
  <div id="wrapper">
  <header>
  <h2>Options</h2>
  <span class="modalClose">x</span>
  </header>
    <div class="modalBody">
    	<p>
        <label>
     	<input name="" checked="checked" type="checkbox" id="audioCheckbox" value="Sounds" /> Notification Sounds
        </label>
    	</p>        
  </div>        
    </div><!-- End #modalBody -->
