var express = require('express');
var app = express();
var path = require('path');

// viewed at http://localhost:8080
app.get('*.js', function(req, res) {
    var scriptFile = path.join(__dirname + '/js' + req.originalUrl);
    console.log('-- Passing script: ' + scriptFile);
    res.sendFile(scriptFile);
});

app.get('*.css', function(req, res) {
    var styleSheet = path.join(__dirname + '/css' + req.originalUrl);
    console.log('-- Passing style: ' + styleSheet);
    res.sendFile(styleSheet);
});

app.get('*.(otf|ttf)$', function(req, res) {
    var fontFile = path.join(__dirname + '/fonts' + req.originalUrl);
    console.log('-- Passing font: ' + fontFile);
    res.sendFile(fontFile);
});

app.get('*.(gif|jpg|jpeg|tiff|png)$', function(req, res) {
    var imgFile = path.join(__dirname + '/images' + req.originalUrl);
    console.log('-- Passing img: ' + imgFile);
    res.sendFile(imgFile);
});

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});
app.listen(8080);

console.log('Listening at http://localhost:8080');